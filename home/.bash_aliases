# General
alias d="docker"
alias dc="docker-compose"

# Git
alias g="git"
alias ga="git add"
alias gaa="git add --all"
alias gb="git branch"
alias gc="git commit"
alias gca="git commit --amend"
alias gco="git checkout"
alias gf="git fetch"
alias glog="git log"
alias glg="git lg"
alias gl="git pull"
alias gp="git push"
alias gpf="git push --force-with-lease"
alias gst="git status"
alias gsu="git stash -u"
alias gsp="git stash pop"
alias gsa="git stash apply"
alias gsc="git stash clear"
alias gd="git diff"

# ls
alias l="ls"
alias ls="ls --color"
