{ pkgs, ... }:

{
  imports =
    [
      ./packages.nix
      ./programs/git.nix
    ];

  programs.htop.enable = true;
  programs.vscode = {
    enable = true;
    extensions = [ ];
  };
}