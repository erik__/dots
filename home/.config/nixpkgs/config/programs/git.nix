{ pkgs, ... }:

{
  programs.git = {
    enable = true;
    package = pkgs.gitAndTools.gitFull;

    userEmail = "erik.aouizerate@mintset.io";
    userName = "Erik Aouizerate";

    aliases = {
      co = "checkout";
      pushf = "push --force-with-lease --force-if-includes";
    };

    ignores = [
      ".DS_Store"
      ".svn"
      "*~"
      "*.swp"
      "*.rbc"
      ".watsonrc"
      ".idea"
      ".vscode"
    ];
  };
}