{ pkgs, lib, ... }:

{
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "1password"
    "1password-cli"

    "ngrok"
    "vscode"

    "google-chrome"
    "slack"
  ];

  home.packages = with pkgs; [
    wget
    curl
    ngrok

    zsh
    oh-my-zsh
    zsh-syntax-highlighting
    zsh-powerlevel10k
    
    _1password
    _1password-gui

    brave
    google-chrome
    slack
    libreoffice
    filezilla
    gimp

    vlc
    vscode

    nodejs-16_x
    cypress
    podman
    podman-compose

  ];
}