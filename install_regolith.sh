#!/bin/sh

BASEDIR=$(cd "$(dirname "$0")"; pwd)/home/.config/regolith2/i3/config.d
HOME_I3=$HOME/.config/regolith2/i3/config.d


# -- [[ Linking ]] -------------------------------------------------------------
echo "Linking configuration files..."

wget -qO - https://regolith-desktop.org/regolith.key | \
gpg --dearmor | sudo tee /usr/share/keyrings/regolith-archive-keyring.gpg > /dev/null

echo deb "[arch=amd64 signed-by=/usr/share/keyrings/regolith-archive-keyring.gpg] \
https://regolith-desktop.org/release-ubuntu-jammy-amd64 jammy main" | \
sudo tee /etc/apt/sources.list.d/regolith.list

sudo apt update
sudo apt install regolith-desktop i3xrocks-battery i3xrocks-memory i3xrocks-wifi i3xrocks-temp i3xrocks-disk-capacity i3xrocks-rofication gnome-screenshot
sudo apt upgrade
sudo apt remove regolith-i3-session regolith-i3-navigation regolith-i3-next-workspace 

sudo rm -rf /usr/share/regolith/i3/config.d/*

mkdir -p /home/.config/regolith2/i3/

ln -sfF $BASEDIR $HOME_I3
