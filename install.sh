#!/bin/sh

[[ "$1" == "-f" ]] && FORCE=true
SKIP=false

platformlink() {
  target=$1
  link=$2

  ln -sfFT $target $link
}

safelink()
{
  $SKIP && return

  target=$1
  link=$2

  if [ $FORCE ]; then
    DO_LINK=true
  else
    if [ -d $link -o -f $link ]; then
      DO_LINK=false

      echo -n "$(tput setaf 3)'$link' already exists, do you want to replace it? ([y]es/[N]o/[a]ll/[s]kip) $(tput sgr0)"

      read answer
      case $answer in
        "yes"|"y")
          DO_LINK=true
          ;;
        "all"|"a")
          FORCE=true
          DO_LINK=true
          ;;
        "skip"|"s")
          SKIP=true
          return
          ;;
        *)
          DO_LINK=false
          ;;
      esac
    else
      DO_LINK=true
    fi
  fi

  $DO_LINK && platformlink $target $link
}

safeinstall() {
  package=$1

    sudo apt install $package
}

BASEDIR=$(cd "$(dirname "$0")"; pwd)/home


# -- [[ Linking ]] -------------------------------------------------------------
echo "Linking configuration files..."

# .config directories
# [[ -d ~/.config ]] || mkdir ~/.config
# copy folder tree

# cp -as $BASEDIR/.config ~/ 


safelink $BASEDIR/.gitconfig $HOME/.gitconfig
safelink $BASEDIR/.gitignore $HOME/.gitignore
safelink $BASEDIR/.ssh/config $HOME/.ssh/config
# safelink $BASEDIR/.bash_aliases $HOME/.bash_aliases
safelink $BASEDIR/.zshrc $HOME/.zshrc
safelink $BASEDIR/.p10k.zsh $HOME/.p10k.zsh

# copy p10k fonts
mkdir -p $HOME/.local/share/fonts/
cp $BASEDIR/.local/share/fonts/* $HOME/.local/share/fonts/

# synaptics conf
sudo cp $BASEDIR/etc/X11/xorg.conf.d/71-synaptics.conf /etc/X11/xorg.conf.d/71-synaptics.conf


# -- [[ Package / plugins installation ]] --------------------------------------
echo
echo -n "Do you want to check packages? ([y]es/[N]o) "

read answer
case $answer in
  "yes"|"y")
    safeinstall curl
    safeinstall htop
    safeinstall zsh
    chsh -s $(which zsh)

    safeinstall xcwd
    sudo apt install xserver-xorg-input-synaptics

    # safeinstall vim

    sudo snap install sublime-text --classic
    sudo snap install code --classic
    sudo snap install vlc
    sudo snap install slack

    # 1password cli
    curl -sS https://downloads.1password.com/linux/keys/1password.asc | sudo gpg --dearmor --output /usr/share/keyrings/1password-archive-keyring.gpg
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/1password-archive-keyring.gpg] https://downloads.1password.com/linux/debian/$(dpkg --print-architecture) stable main" | sudo tee /etc/apt/sources.list.d/1password.list
    sudo mkdir -p /etc/debsig/policies/AC2D62742012EA22/
    curl -sS https://downloads.1password.com/linux/debian/debsig/1password.pol | sudo tee /etc/debsig/policies/AC2D62742012EA22/1password.pol
    sudo mkdir -p /usr/share/debsig/keyrings/AC2D62742012EA22
    curl -sS https://downloads.1password.com/linux/keys/1password.asc | sudo gpg --dearmor --output /usr/share/debsig/keyrings/AC2D62742012EA22/debsig.gpg
    sudo apt update && sudo apt install 1password-cli
    
    # 1password
    echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/1password-archive-keyring.gpg] https://downloads.1password.com/linux/debian/amd64 stable main' | sudo tee /etc/apt/sources.list.d/1password.list
    sudo mkdir -p /etc/debsig/policies/AC2D62742012EA22/
    curl -sS https://downloads.1password.com/linux/debian/debsig/1password.pol | sudo tee /etc/debsig/policies/AC2D62742012EA22/1password.pol
    sudo mkdir -p /usr/share/debsig/keyrings/AC2D62742012EA22
    curl -sS https://downloads.1password.com/linux/keys/1password.asc | sudo gpg --dearmor --output /usr/share/debsig/keyrings/AC2D62742012EA22/debsig.gpg
    sudo apt update && sudo apt install 1password

    # brave
    sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
    echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
    sudo apt update
    sudo apt install brave-browser

    # more gnome settings
    # sudo apt install gnome-tweaks

    # gnome extension
    # sudo apt install gnome-shell-extensions
    # sudo apt install gnome-shell-extension-manager

    # for system monitor gnome extension 
    # sudo apt install gir1.2-gtop-2.0 gir1.2-nm-1.0 gir1.2-clutter-1.0 gnome-system-monitor


#    sudo apt install nomachine
#    sudo apt install gnome-shell-extensions-gpaste gpaste
#    sudo apt install fzf
#    sudo apt install bat
#    sudo apt-get install tig

    # firefox video driver
#    sudo apt install libavcodec-extra

    # vlc codec mp4 (and more maybe...)
#    sudo apt install libdvdnav4 libdvd-pkg gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly libdvd-pkg
#    sudo apt install ubuntu-restricted-extras

    # nvm
   curl -fsSL https://fnm.vercel.app/install | bash
    # node / npm by nvm 
   fnm install 16.15.0
   fnm use 16.15.0
   npm i -g yarn

    # oh-my-zsh
   sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

    # oh-my-zsh plugins
   git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
   git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/powerlevel10k
   sudo apt install fzf

    # google-chrome
   sudo sh -c 'echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list'
   wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
   sudo apt-get update
   sudo apt-get install google-chrome-stable

    # android
#    sudo apt install android-sdk
#    sudo apt-get install openjdk-8-jdk  

    # docker
    sudo apt install docker
    sudo apt install docker-compose

    # Corsair mouse driver options 
#    sudo apt install ckb-next

    echo "$(tput setaf 2)All dependencies are up to date$(tput sgr0)"
    ;;
  *)
    echo "$(tput setaf 3)Packages update skipped$(tput sgr0)"
    ;;
esac

