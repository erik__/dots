#!/bin/sh

wget -O - https://nixos.org/nix/install | bash

. $HOME/.nix-profile/etc/profile.d/nix.sh

nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
nix-channel --update

export NIX_PATH=$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels${NIX_PATH:+:$NIX_PATH}

nix-shell '<home-manager>' -A install

nix-shell -p git --run 'git clone https://gitlab.com/erik__/dots.git'

sed -i '$ d' ~/.config/nixpkgs/home.nix
echo "
 imports =
    [
      ./config/home.nix
    ];
}
" >> ~/.config/nixpkgs/home.nix


ln -sfF ~/dots/home/.config/nixpkgs/config ~/.config/nixpkgs/config

home-manager switch

./install_regolith.sh

./load-dconf.sh